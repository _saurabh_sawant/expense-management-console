package create_group;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import homescreen2.HomeScreen2;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class CreateGroupController {
	@FXML
	private TextField groupName;
	
	@FXML
	private TextField type;

	@FXML
	private Button createGroup;
	
	public void createGroup(ActionEvent event) throws IOException {
		
		// The message that is going to be sent to the server
				// using the POST request
				final String messageContent = "{\n" + "\"groupName\"" + ":\"" + groupName.getText() + "\", \r\n" + "\"type\"" + ":\""
						+ type.getText() +  "\" \r\n"+ "\n}";
				
				// Printing the message
				System.out.println(messageContent);

				// URL of the API or Server
				String url = "http://localhost:8080/groups/api/v1/create";
				URL urlObj = new URL(url);
				HttpURLConnection postCon = (HttpURLConnection) urlObj.openConnection();
				postCon.setRequestMethod("POST");
				postCon.setRequestProperty("userId", "abcdef");
				
				// Setting the message content type as JSON
				postCon.setRequestProperty("Content-Type", "application/json");
				postCon.setDoOutput(true);
				
				// for writing the message content to the server
				OutputStream osObj = postCon.getOutputStream();
				osObj.write(messageContent.getBytes());
				
				// closing the output stream
				osObj.flush();
				osObj.close();
				int respCode = postCon.getResponseCode();
				System.out.println("Response from the server is: \n");
				System.out.println("The POST Request Response Code :  " + respCode);
				System.out.println("The POST Request Response Message : " + postCon.getResponseMessage());
				if (respCode == HttpURLConnection.HTTP_CREATED) {
					// reaching here means the connection has been established
					// By default, InputStream is attached to a keyboard.
					// Therefore, we have to direct the InputStream explicitly
					// towards the response of the server
					InputStreamReader irObj = new InputStreamReader(postCon.getInputStream());
					BufferedReader br = new BufferedReader(irObj);
					String input = null;
					StringBuffer sb = new StringBuffer();
					while ((input = br.readLine()) != null) {
						sb.append(input);
					}
					br.close();
					postCon.disconnect();
					// printing the response
					
					System.out.println(sb.toString());
					new HomeScreen2().show();
					
				} else {
					// connection was not successful
					System.out.println("POST Request did not work.");
					//new CreateGroup().show();
				}
			}
}
