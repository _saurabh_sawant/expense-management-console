package homescreen;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login.Login;
import sign_up.SignUp;


public class HomeScreenController {
	@FXML
	private Button signUp;
	
	@FXML
	private Button login;
	
	public void signUp(ActionEvent event) {
		new SignUp().show();
	}
	
	public void login(ActionEvent event) {
		new Login().show();
	}	
}
