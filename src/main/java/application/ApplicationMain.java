package application;

import homescreen.HomeScreen;
import javafx.application.Application;
import javafx.stage.Stage;
import stage.StageMaster;

public class ApplicationMain extends Application{
	public static void main(String args[]) {
		//DbUtil.createDbConnection();
		launch(args);
		
	}
	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
		new HomeScreen().show();
	}

}