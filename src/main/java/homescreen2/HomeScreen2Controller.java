package homescreen2;


import create_group.CreateGroup;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;


public class HomeScreen2Controller {
	@FXML
	private Button createGroup;
	
	@FXML
	private Button createFriend;
	
	public void createGroup(ActionEvent event) {
		new CreateGroup().show();
	}
	
	public void createFriend(ActionEvent event) {
		new CreateGroup().show();
	}	
}
